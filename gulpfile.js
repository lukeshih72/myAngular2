let gulp = require('gulp');
let fs = require('fs');
//var child_process = require('child_process');
let AWS = require('aws-sdk');
const path = require('path');
const globby = require('globby');
const mime = require('mime-types');

//env
let s3 = new AWS.S3({
  useDualstack: true,
  region: "us-west-2"
});
let BUCKET = 'lukedemo.dev.thingspro.xyz';

gulp.task('aws', () => {
  console.log('Gulp Task: AWS');

  const distPath = path.join('.', 'dist');

  globby(distPath, {
    expandDirectories: {
        files: ['*']
    }
  }).then(files => {
    files.map(file => {
      let ext = path.extname(file);
      let newfile = path.join(file.replace(distPath+'/', ''));
      const readStream = fs.createReadStream(file);
      
      var params = {
        Bucket: BUCKET,
        ACL: "public-read",
        Key: newfile,
        ContentType: mime.contentType(ext),
        Body: readStream
      };
      s3.upload(params, function(err, data) {
        console.log(err, data);
      });
    });
  });

});

gulp.task('default', []);
