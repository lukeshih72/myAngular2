import { Component } from '@angular/core';

@Component({
  selector: 'loopback',
  template: `
    <input #box (keyup.enter)="OnEnter(box.value)"
    (blur)="OnUpdate(box.value)">
    <p>{{value}}</p>
  `
})

export class LoopbackComponent{
    value = "";

    OnKeyUp(value: string) {
        this.value += value + " | ";
    }

    OnEnter(value: string) {
        this.value = value;
    }

    OnUpdate(value: string) {
        this.value = value+"jumimi";
    }
}