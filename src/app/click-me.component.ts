import { Component } from '@angular/core';

@Component({
  selector: 'click-me',
  template: `
    <button (click)="OnClickMe()">Click me!</button>
    {{clickMessage}}
  `
})

export class ClickMeComponent{
    clickMessage = '';

    OnClickMe() {
        this.clickMessage = 'You are my hero!';
    }

}