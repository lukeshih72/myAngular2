import { Component } from '@angular/core';
import { Hero } from './hero';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent {
  title: string;
  myHero: object;
  heros: object;


  constructor(){
    this.title = 'Tour of Heros';    
    this.heros = [
      new Hero(1, 'Windstorm', 'a'),
      new Hero(13, 'Bombasto', 'a'),
      new Hero(15, 'Magneta', 'a'),
      new Hero(20, 'Tornadoo', 'a')
    ];
    this.myHero = this.heros[0];
  }

}