import { Component } from '@angular/core';

@Component({
  selector: 'keyup',
  template: `
    <input (keyup)="OnKey($event)">
    <p>{{value}}</p>
  `
})

export class KeyUpComponent_v1{
    value = '';

    OnKey(event: any) {
        //this.value += event.target.value + " | ";
        this.value = event.target.value;
    }

}